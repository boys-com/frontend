FROM maven:3.6.3-jdk-11
COPY src /app/src
COPY pom.xml /app
WORKDIR /app
RUN mvn clean package
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "/app/target/frontend-1.1.0-SNAPSHOT.jar"]

